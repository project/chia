# Chia module for Drupal

![Logo chia](assets/logo_chia.png)

Module designed for Chia™ and Drupal from version 9 onwards.

- The main module registers `chia` and registers main XCH config entities and services  (i.e. RPC @see [Chia™ RPC service documentation](https://docs.chia.net/rpc/))
- Submodule will be dedicated to each feature we are interested with

## Main module

**Features:**

- Wallet management (requires access to your wallet RPC)
- Node management (requires access to your node RPC)
- Status notification changes (uptime/downtime), requires cron;

_As manipulating the wallet/nodes RPC API requires your private key, I recommend using a fresh private key if you plan to use this module._

## Drupal commerce

Shops using the `chia_commerce` module will be able to sell their products for `$xch`.

**Features :**

- `$xch` exchange rate : you set your prices in your local currency (the majority of FIAT currencies are supported), the conversion to $xch is done regularly in drupal cron,
- Transaction expiry: currently, `$xch` price are more volatile than those of your local currency, so you manage the end of life of transactions (customer has X minutes to validate the transaction),

## Installation

**Requirements**

- Drupal 9+
- [Commerce](https://www.drupal.org/project/commerce) if you install `chia_commerce`
- `commerce_payment`
- [Commerce exchanger](https://www.drupal.org/project/commerce_exchanger)
- Recommended : [Commerce currency resolver](https://www.drupal.org/project/commerce_currency_resolver) 

**Install from composer**

```php
composer require drupal/chia
```

**Note :** cron should be run every 5-15 minutes if you use `commerce_payment` module ([how-to configure cronjob](https://www.drupal.org/docs/7/setting-up-cron/configuring-cron-jobs-using-the-cron-command)).

## Roadmap:

- Documentation
- Refund management (not yet implemented in `chia_commerce`)
- Wallet creation,
- NFT management in the core `chia` module
- NFT minting (Only for offer files, invoices related to `chia_commerce` module)
- Offerfiles payment/invoice in commerce

## Informations:

All assets are Chia Inc. properties
Please note this module is not affiliated with Chia Inc. in any way.

Goal is to bring `chia` blockchain more mainstream, feel free to get in touch and participate in development of this module.

I like coffee : `xch1uxd8y485yrv7tvuv03ru36ltx7t43lpsxjr2x5zua5zw5mkyfu3qfk4peg` :-) 


