<?php

namespace Drupal\chia_commerce\Services;

use Drupal\chia\Entity\Wallet;
use Drupal\chia\Exceptions\ChiaException;
use Drupal\chia\Services\WalletRpc;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Service description.
 */
class PaymentService {

  use StringTranslationTrait;

  /**
   * If a Payment has more than MAX_TRANSACTIONS_PER_PAYMENT
   * Exception will be thrown, and payment will be voided.
   * Something seems not legit here.
   */
  const MAX_TRANSACTIONS_PER_PAYMENT = 20;
  /**
   * The chia.rpc service.
   *
   * @var \Drupal\chia\Services\WalletRpc
   */
  protected $rpc;

  /**
   * @var Connection
   */
  protected $database;

  /**
   * Constructs a PaymentService object.
   *
   * @param \Drupal\chia\Services\WalletRpc $rpc
   *   The chia.rpc service.
   */
  public function __construct(WalletRpc $rpc, Connection $database) {
    $this->rpc = $rpc;
    $this->database = $database;
  }

  public function voidPayment(Payment $payment) {
    try {
      $payment->setState('voided');
      $payment->save();
    } catch (\Exception $exc) {
      \Drupal::logger('chia_commerce')
        ->critical($this->t("Unable to void payment with ID @id :  @exception", [
          '@id' => $payment->id(),
          '@exception' => $exc->getMessage(),
        ]));
    }
  }

  public function confirmPayment(Payment $payment, string $transactionID) {
    try {
      $payment->setState('completed');
      $payment->set('tx', $transactionID);
      $payment->save();
    } catch (\Exception $exc) {
      \Drupal::logger('chia_commerce')
        ->critical($this->t("Unable to confirm payment with ID @id", ['@id' => $payment->id()]));
    }
  }

  /**
   * Returns true if transaction is completed on blockchain.
   *
   * @param \Drupal\commerce_payment\Entity\Payment $payment
   *
   * @return \DateInterval|bool
   *   Time left if pending/partially paid, bool if completed/voided
   */
  public function isTransactionComplete(Payment $payment): mixed {
    if ($payment->getState()->getId() === 'voided') {
      return FALSE;
    }
    if ($payment->getState()->getId() === 'completed') {
      return TRUE;
    }
    try {
      $expires = new DrupalDateTime();
      $expires->setTimestamp($payment->expiration_date->value);
      $now = new DrupalDateTime();
      $interval = $now->diff($expires);
      if ($expires->getTimestamp() < $now->getTimestamp()) {
        $this->voidPayment($payment);
      }
      else {
        $infos = $this->getPaymentRemaining($payment);
        if($infos['left'] <= 0){
            $this->confirmPayment($payment, implode(',', $infos['transactions']));
            return TRUE;
        }
        return $interval;
      }
    } catch (\Exception $exc) {
      // Any kind of exception : payment is voided
      $this->voidPayment($payment);
      \Drupal::logger('chia_commerce')->warning($this->t('Unhandled exception, payment @payment has been voided : @exception', [
        '@payment' => $payment->id(),
        '@exception' => $exc->getMessage(),
      ]));
    }
    return FALSE;
  }

  /**
   * @return array
   *  array(
   *   'left' => meft to pay
   *   'transactions' => coins names
   * )
   */
  public function getPaymentRemaining(Payment $payment) {
     if ($payment->getState()->getId() !== 'voided' && $payment->getState()->getId() !== 'completed') {
       $wallet = Wallet::load($payment->getPaymentGateway()
         ->getPluginConfiguration()['wallet']);
       $walletId = $payment->getPaymentGateway()
         ->getPluginConfiguration()['wallet_id'];
       $transactions = $this->rpc->getLastTransactions($wallet, $walletId, $payment->xch_target_address->value);
       $amount = (int) ($payment->getAmount()->getNumber() * 1000000000000);
       $payed = 0;
       $tx = [];
       foreach ($transactions->transactions as $transaction) {
         $samePayment = $this->findPaymentByTransaction($transaction->name);
         if (!$samePayment) {
           $payed = $payed + $transaction->amount;
           $tx[] = $transaction->name;
         }
       }
       if (count($tx) > self::MAX_TRANSACTIONS_PER_PAYMENT) {
        throw new ChiaException($this->t('Maximum number of transactions reached for this payment.'));
       }
       $remaining = $amount - $payed;
       $remainingStored = (int) $payment->get('remaining')->value;
       if($remainingStored !== $remaining) {
         $payment->set('remaining', $remaining);
         if($remaining > 0) {
           $payment->setState('partially_completed');
         } else {
           $payment->setCompletedTime($transaction->created_at_time);
           $payment->setState('completed');
         }
         $payment->save();
       }
       return [
         'left' => $amount - $payed,
         'transactions' => $tx,
       ];
     }
     throw new ChiaException($this->t('Payment voided or completed, nothing left to pay.'));
  }

  /**
   * @param $options
   *
   * @return \Drupal\commerce_payment\Entity\Payment|bool
   */
  public function findPaymentByTransaction($transactionId): mixed {
    $select = $this->database->select('commerce_payment', 'c');
    $select->fields('c', ['payment_id']);
    $select->join('commerce_payment__tx', 'tx', 'tx.entity_id = c.payment_id');
    $select->condition('tx.tx_value', '%'.$transactionId.'%', 'LIKE');
    $statement = $select->execute();
    $payment = FALSE;
    if ($o = $statement->fetchObject()) {
      $payment = Payment::load($o->payment_id);
    }
    return $payment;
  }

}
