<?php

namespace Drupal\chia_commerce\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Refreshes chia transaction checkout.
 */
class RefreshCheckoutCommand implements CommandInterface
{
  /**
   * @inheritDoc
   */
  public function render()
  {
    return [
      'command' => 'refreshCheckout',
    ];
  }
}
