<?php

namespace Drupal\chia_commerce\Controller;

use Drupal\chia_commerce\Ajax\RefreshCheckoutCommand;
use Drupal\chia\Exceptions\ChiaException;
use Drupal\chia_commerce\Services\PaymentService;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\chia\Services\WalletRpc;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Chia routes.
 */
class CheckoutController extends ControllerBase {

  /**
   * The chia.rpc service.
   *
   * @var \Drupal\chia\Services\WalletRpc
   */
  protected $rpc;

  /**
   * @var \Drupal\chia_commerce\Services\PaymentService
   */
  protected $paymentService;

  /**
   * The controller constructor.
   *
   * @param \Drupal\chia\Services\WalletRpc $rpc
   *   The chia.rpc service.
   */
  public function __construct(WalletRpc $rpc, PaymentService $paymentService) {
    $this->rpc = $rpc;
    $this->paymentService = $paymentService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('chia.rpc'),
      $container->get('chia_commerce.payment')
    );
  }

  /**
   * Builds the response.
   */
  public function checkoutPayment(Payment $payment, $js = 'nojs') {
    $build = [];
    if ($payment->getPaymentGateway()->getPluginId() !== 'chia') {
      throw new ChiaException($this->t('Invalid plugin for this payment.'));
    }
    $response = new AjaxResponse();
    $transactionComplete = $this->paymentService->isTransactionComplete($payment);

    if ($transactionComplete === TRUE) {
      $build['message'] = [
        'status' => [
          '#theme' => 'status_messages',
          '#status_headings' => [
            'status' => t('Status message'),
            'error' => t('Error message'),
            'warning' => t('Warning message'),
          ],
          '#message_list' => [
            'status' => [
              '#markup' => $this->t('Payment has been processed : you can check coins ID : @tx', [
                '@tx' => str_replace(',', ' ', $payment->get('tx')->value),
              ]),
            ],
          ],
        ],
      ];
    }
    else {
      if ($transactionComplete === FALSE) {
        $build['message'] = [
          '#theme' => 'status_messages',
          '#status_headings' => [
            'status' => t('Status message'),
            'error' => t('Error message'),
            'warning' => t('Warning message'),
          ],
          '#message_list' => [
            'error' => [
              $this->t('Offer expired, you should checkout again.'),
            ],
          ],
        ];
      }
      else {
        if ($transactionComplete instanceof \DateInterval) {
          $remainingInfos = $this->paymentService->getPaymentRemaining($payment);
          $url = Url::fromRoute('chia_commerce.chia_checkout_payment', ['payment' => $payment->id()]);
          $build['message'] = [
            '#theme' => 'chia_commerce_checkout_payment',
            '#url_refresh' => $url->toString(),
            '#order_number' => $payment->getOrder()->getOrderNumber(),
            '#xch_target_address' => $payment->get('xch_target_address')->value,
            '#expiration' => $this->t("@hours hours, @minutes minutes, @seconds seconds", [
              '@hours' => $transactionComplete->h,
              '@minutes' => $transactionComplete->i,
              '@seconds' => $transactionComplete->s,
            ]),
            '#amount' => ($remainingInfos['left'] / 1000000000000),
            '#attached' => [
              'library' => ['chia/chia_gateway'],
            ],
          ];
          $response->addCommand(new RefreshCheckoutCommand());
        }
      }
    }
    $response->addCommand(new ReplaceCommand('#xch-payment-check', $build));
    return $response;
  }

}
