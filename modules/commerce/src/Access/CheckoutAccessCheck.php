<?php

namespace Drupal\chia_commerce\Access;

use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access checker for chia transaction checkout.
 */
class CheckoutAccessCheck implements AccessInterface
{
  public function access(AccountInterface $account)
  {
    /* @var $payment Payment */
    $payment = \Drupal::routeMatch()->getParameter('payment');
    return ($payment->getOrder()->getCustomerId() === $account->id())? AccessResult::allowed() :AccessResult::forbidden();
  }

}
