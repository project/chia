<?php

namespace Drupal\chia_commerce\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the manual payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_chia",
 *   label = @Translation("Chia"),
 *   workflow = "payment_chia",
 * )
 */
class PaymentChia extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [
      'remaining' => BundleFieldDefinition::create('string')
        ->setLabel($this->t('Remaining'))
        ->setTranslatable(FALSE)
        ->setRequired(TRUE),
      'expiration_date' => BundleFieldDefinition::create('integer')
        ->setLabel($this->t("Expiration date")),
      'xch_target_address' => BundleFieldDefinition::create('string')
        ->setLabel($this->t("Chia target address"))
        ->setDescription($this->t("Address where the customer is supposed to send XCH"))
        ->setTranslatable(FALSE)
        ->setReadOnly(TRUE)
        ->setRequired(TRUE),
      'tx' => BundleFieldDefinition::create('string')
        ->setLabel($this->t("Transaction IDs"))
        ->setSettings([
          'max_length' => 15000,
        ])
        ->setDescription($this->t("Transaction IDs on the chia blockchain used to pay"))
        ->setTranslatable(FALSE),
    ];
  }

}
