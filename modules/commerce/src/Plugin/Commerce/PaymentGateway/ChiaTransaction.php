<?php

namespace Drupal\chia_commerce\Plugin\Commerce\PaymentGateway;

use DateInterval;
use Drupal;
use Drupal\chia\ChiaConstants;
use Drupal\chia\Entity\Wallet;
use Drupal\chia\Exceptions\ChiaException;
use Drupal\chia\Services\WalletRpc;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsUpdatingStoredPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Exception;
use InvalidArgumentException;

/**
 * Provides the Chia payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "chia",
 *   label = "Payment by chia transaction.",
 *   display_label = "Payment by chia transaction",
 *   modes = {
 *     "transaction" = @Translation("Blockchain transaction"),
 *   },
 *   forms = {
 *     "add-payment" = "Drupal\chia_commerce\PluginForm\ChiaPaymentAddForm",
 *     "receive-payment" =
 *   "Drupal\chia_commerce\PluginForm\PaymentReceiveForm",
 *     "refund-payment" =
 *   "Drupal\commerce_payment\PluginForm\PaymentRefundForm",
 *   },
 *   payment_type = "payment_chia",
 *   requires_billing_information = FALSE,
 * )
 */
class ChiaTransaction extends PaymentGatewayBase implements ManualPaymentGatewayInterface {

  /**
   * @var Wallet
   */
  protected ?Wallet $wallet;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'wallet' => $this->configuration['wallet'],
        'wallet_id' => $this->configuration['wallet_id'],
        'expiration' => $this->configuration['expiration'],
      ] + parent::defaultConfiguration();
  }

  /**
   * @return Wallet|null
   */
  public function getWallet(): ?Wallet {
    if (!isset($this->wallet)) {
      $this->wallet = Wallet::load($this->configuration['wallet']);
    }
    return $this->wallet;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['instructions'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Payment instructions'),
      '#description' => $this->t('Shown the end of checkout, after the customer has placed their order.'),
      '#default_value' => $this->configuration['instructions']['value'],
      '#format' => $this->configuration['instructions']['format'],
    ];

    $wallets = Wallet::loadMultiple();
    $options = [];
    foreach ($wallets as $wallet) {
      $options[$wallet->id()] = $wallet->label();
    }
    $form['wallet'] = [
      '#type' => 'select',
      '#title' => $this->t('Wallet'),
      '#options' => $options,
      '#default_value' => $this->configuration['wallet'],
      '#required' => TRUE,
      '#description' => $this->t("You must create a wallet in order to configure this payment gateway."),
      '#ajax' => [
        'callback' => [$this, 'walletChangeHandler'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'wallet-id-selector-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading wallets...'),
        ],
      ],
    ];
    $wallet = $this->getWallet();
    $form['wallet_id'] = $this->renderWalletIDSelector($wallet);

    $form['expiration'] = [
      '#type' => 'number',
      '#title' => 'Transaction expiration time',
      '#description' => $this->t('As the rate of $chia can fluctuate enormously, you can define a duration (in minutes) beyond which the transaction will be voided, the customer will have to redo a checkout (default: 15 minutes)'),
      '#default_value' => $this->configuration['expiration'] ?? '15',
    ];
    return $form;
  }

  /**
   * @param ?Wallet $wallet
   *
   * @return array|string[]
   */
  protected function renderWalletIDSelector(Wallet $wallet = NULL): array {
    $rpc = Drupal::service('chia.rpc');
    $selector = [
      '#prefix' => '<div id="wallet-id-selector-wrapper">',
      '#suffix' => '</div>',
    ];
    try {
      $options = [];
      if ($wallet) {
        $walletIds = $rpc->getWallets($wallet);
        foreach ($walletIds->wallets as $walletId) {
          if ($walletId->type === ChiaConstants::WALLET_TYPE_CAT) {
            $options[$walletId->id] = $walletId->name . ' (' . $walletId->id . ')';
          }
        }
      }
      $selector = $selector + [
          '#type' => 'select',
          '#description' => $this->t('Wallet that will be used by this payment gateway. (Options are formally the result of "chia wallet show "command showing only cat wallets.)'),
          '#options' => $options,
          '#required' => TRUE,
          '#default_value' => $this->configuration['wallet_id'],
          '#states' => [
            'invisible' => [
              ':select[name="wallet"]' => ['value' => ''],
            ],
          ],
        ];
    } catch (Exception $exc) {
      $selector = $selector + [
          'status' => [
            '#type' => 'status_messages',
            '#status_headings' => [
              'status' => t('Status message'),
              'error' => t('Error message'),
              'warning' => t('Warning message'),
            ],
            '#message_list' => [
              'error' => [
                $this->t('Unable get a wallet list, please check your wallet status.'),
                $this->t($exc->getMessage()),
              ],
            ],
          ],
        ];
    }
    return $selector;
  }


  /**
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  public function walletChangeHandler(array &$form, FormStateInterface $form_state): array {
    $values = $form_state->getValues();
    $wallet = Wallet::load($values['configuration']['chia']['wallet']);
    $form['wallet_id'] = $this->renderWalletIDSelector($wallet);
    return $form['wallet_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['wallet'] = $values['wallet'];
      $this->configuration['wallet_id'] = $values['wallet_id'];
      $this->configuration['expiration'] = $values['expiration'];
      $this->configuration['instructions'] = $values['instructions'];
    }
  }

  /**
   * Asserts that the payment state matches one of the allowed states.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param string[] $states
   *   The allowed states.
   *
   * @throws \InvalidArgumentException
   *   Thrown if the payment state does not match the allowed states.
   */
  protected function assertPaymentState(PaymentInterface $payment, array $states) {
    $state = $payment->getState()->getId();
    if (!in_array($state, $states)) {
      throw new InvalidArgumentException(sprintf('The provided payment is in an invalid state ("%s").', $state));
    }
  }

  /**
   * {@inheritdoc}
   * @throw HardDeclineException
   */
  public function createPayment(PaymentInterface $payment, $capture = FALSE): void {
    try {
      $this->assertPaymentState($payment, ['new', 'pending']);
      $amount = $payment->getAmount()->getNumber() * 1000000000000;
      $now = new DrupalDateTime();
      $expires = new DrupalDateTime();
      $expires->add(new DateInterval(sprintf('PT%dM', $this->configuration['expiration'])));
      /* @var $rpc WalletRpc */
      $rpc = Drupal::service('chia.rpc');
      $payment->setAvsResponseCode('I');
      $payment->setAvsResponseCodeLabel($this->t('Address not verified (International only)'));
      $payment->set('remaining', $amount);
      $payment->set('xch_target_address', $rpc->getNextAddress($this->getWallet(), $this->configuration['wallet_id']));
      $payment->set('expiration_date', $expires->getTimestamp());
      $payment->setAuthorizedTime($now->getTimestamp());
      $payment->setExpiresTime($expires->getTimestamp());
      $status = $capture ? 'completed' : 'pending';
      $payment->setState($status);
      $payment->save();
    } catch (Exception $exc) {
      Drupal::logger('chia_commerce')
        ->error("Payment creation failed : " . $exc->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL): bool {
    $this->assertPaymentState($payment, ['pending']);
    $rpc = Drupal::service('chia.rpc');
    $wallet = Wallet::load($this->configuration['wallet']);
    $response = $rpc->getLastTransactions($wallet, $this->configuration['wallet_id'], $payment->get('xch_target_address'));
    $bReceived = FALSE;
    if (isset($response->transactions)) {
      foreach ($response->transactions as $transaction) {
        $number = $number * 1000000000000; // transform into mojos.
        if ($transaction->confirmed === 1 && $transaction->amount === $number) {
          $payment->setState('completed');
          $payment->setAmount($amount);
          $bReceived = TRUE;
          $payment->save();
        }
      }
    }
    return $bReceived;
  }

  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];
    if (!empty($this->configuration['instructions']['value'])) {
      $instructions = [
        '#type' => 'processed_text',
        '#text' => $this->configuration['instructions']['value'],
        '#format' => $this->configuration['instructions']['format'],
      ];
    }
    return $instructions;
  }

  public function voidPayment(PaymentInterface $payment) {
    $payment->setState('voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $operations = [];
    $operations['receive'] = [
      'title' => $this->t('Receive'),
      'page_title' => $this->t('Receive payment'),
      'plugin_form' => 'receive-payment',
      'access' => $payment->getState()->getId() === 'pending' || $payment->getState()->getId() === 'partially_completed',
    ];
    $operations['void'] = [
      'title' => $this->t('Void'),
      'page_title' => $this->t('Void payment'),
      'plugin_form' => 'void-payment',
      'access' => $this->canVoidPayment($payment),
    ];
    $operations['refund'] = [
      'title' => $this->t('Refund'),
      'page_title' => $this->t('Refund payment'),
      'plugin_form' => 'refund-payment',
      'access' => $this->canRefundPayment($payment),
    ];

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function canVoidPayment(PaymentInterface $payment) {
    return in_array($payment->getState()->getId(), ['new', 'pending']);
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    return in_array($payment->getState()->getId(), [
      'partially_refunded',
      'partially_completed',
      'completed',
    ]);
  }

  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

}
