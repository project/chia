<?php

namespace Drupal\chia_commerce\Plugin\Commerce\ExchangerProvider;

use Drupal\commerce_exchanger\Annotation\CommerceExchangerProvider;
use Drupal\commerce_exchanger\Plugin\Commerce\ExchangerProvider\ExchangerProviderRemoteBase;
use GuzzleHttp\RequestOptions;

/**
 * Provides EuropeanCentralBank.
 *
 * @CommerceExchangerProvider(
 *   id = "coingecko_chia",
 *   label = "Coingecko Chia",
 *   display_label = "Coingecko Chia",
 *   historical_rates = TRUE,
 *   enterprise = TRUE,
 *   api_key = FALSE
 * )
 */
class CoingeckoExchanger extends ExchangerProviderRemoteBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cron' => 1,
      'api_key' => '',
      'auth' => [],
      'use_cross_sync' => FALSE,
      'enterprise' => TRUE,
      'demo_amount' => 100,
      'base_currency' => 'XCH',
      'refresh_once' => FALSE,
      'manual' => FALSE,
      'mode' => 'live',
      'transform_rates' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function apiUrl() {
    return 'https://api.coingecko.com/api/v3/simple/price';
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteData($base_currency = 'XCH') {
    $data = [];
    //if($base_currency === 'XCH')
    {
      $currencies = array_map('strtolower', array_keys($this->getCurrencies()));
      $options = [
        RequestOptions::QUERY => [
          'ids' => 'chia',
          'vs_currencies' => implode(',', $currencies)
        ],
        RequestOptions::HEADERS => [
          'Accept' => 'application/json'
        ],
      ];
      try {
        $response = $this->apiClient($options);
        $json = json_decode($response);
        $chiaDatas = (array) $json->chia;
        foreach ($chiaDatas as $currency => $value) {
            $data[strtoupper($currency)] = $value;
        }
      } catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }
    return $data;
  }

}
