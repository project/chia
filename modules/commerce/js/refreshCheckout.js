(function ($, Drupal) {
  Drupal.AjaxCommands.prototype.refreshCheckout = function (ajax, response, status) {
    var _wrapper = $('#xch-payment-check');
    setTimeout(function() {
      Drupal.ajax({url: _wrapper.data('url')}).execute();
    }, 5000);
  }
})(jQuery, Drupal);
