(function($) {
  Drupal.chia_gateway = {
    inited: false
  };
  Drupal.behaviors.chia_gateway = {
    attach: function(context, settings) {
      var _wrapper = $(context).find('#xch-payment-check');
      if(!Drupal.chia_gateway.inited && _wrapper.length > 0) {
        Drupal.chia_gateway.inited = true;
        setTimeout(function() {
          Drupal.ajax({url: _wrapper.data('url')}).execute();
        }, 5000);
      }
    }
  };
})(jQuery);
