<?php

namespace Drupal\Tests\chia_web3\FunctionalJavascript;

use Drupal\chia\Entity\Wallet;
use Drupal\file\Entity\File;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the JavaScript functionality of the Chia web3 module.
 *
 * @coversDefaultClass \Drupal\chia_web3\Form\SettingsForm
 *
 * @group chia_web3
 */
class SettingsTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['chia_web3','file'];

  /**
   * @var \Drupal\user\Entity\User|false
   */
  protected $admin_user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    Role::create([
      'id' => 'auth',
      'label' => 'auth',
    ])->save();
    Role::create([
      'id' => 'auth2',
      'label' => 'auth2',
    ])->save();
    touch('public://cert.pem');
    $fakeCert = File::create([
      'filename' => basename('cert.pem'),
      'uri' => 'public://cert.pem',
      'status' => 1,
      'uid' => 1,
    ]);
    $fakeCert->save();
    Wallet::create([
      'id' => 'wallet',
      'label' => 'wallet',
      'wallet_rpc_url' => 'http://localhost:8555',
      'wallet_key' => [$fakeCert->id()],
      'wallet_cert' => [$fakeCert->id()],
    ])->save();
    $this->admin_user = $this->drupalCreateUser(['administer chia web3 settings','access administration pages', 'administer chia services', 'view chia status page', 'administer chia nodes', 'administer chia wallets']);
  }

  /**
   * Test callback.
   */
  public function testSettingsForm() {
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('admin/config/services/chia/web3-settings');
    $page = $this->getSession()->getPage();
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Chia web3 settings"]');
    $this->assertSession()->elementExists('xpath', '//select[@id="edit-wallet"]');
    $this->assertSession()->elementExists('xpath', '//input[@id="edit-autocreate-users"]');
    $this->assertSession()->elementExists('xpath', '//input[@id="edit-display-button-in-login-form"]');
    $this->assertSession()->elementExists('xpath', '//input[@id="edit-display-link-wallet"]');
    $enableCheckbox = $page->find('xpath', '//input[@id="edit-enable-wallet-connect"]');
    $this->assertEquals(FALSE, $page->find('xpath', '//input[@id="edit-autocreate-users"]')->isVisible());
    $this->assertEquals(FALSE, $page->find('xpath', '//input[@id="edit-display-button-in-login-form"]')->isVisible());
    $this->assertEquals(FALSE, $page->find('xpath', '//input[@id="edit-display-link-wallet"]')->isVisible());
    $this->assertEquals(TRUE, $enableCheckbox->hasAttribute('disabled'));
    $walletSelector = $page->find('xpath', '//select[@id="edit-wallet"]');
    $walletSelector->selectOption('wallet');
    $this->assertEquals(FALSE, $enableCheckbox->hasAttribute('disabled'));
  }

}
