<?php

namespace Drupal\chia_web3\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Refreshes chia transaction checkout.
 */
class ConnectWeb3Command implements CommandInterface
{
  /**
   * @var string
   */
  protected string $randomBytes;
  /**
   * @param string $randomBytes
   */
  public function __construct(string $randomBytes) {
    $this->randomBytes = $randomBytes;
  }
  /**
   * @inheritDoc
   */
  public function render()
  {
    return [
      'command' => 'connectWeb3Command',
      'message' => $this->randomBytes,
    ];
  }
}
