<?php

namespace Drupal\chia_web3\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Checks if a user's public key is unique on the site.
 *
 * @Constraint(
 *   id = "PublicKeyUnique",
 *   label = @Translation("Public key unique", context = "Validation")
 * )
 */
class PublicKeyUnique extends UniqueFieldConstraint {
  public $message = 'The Chia public key %value is already taken.';
}