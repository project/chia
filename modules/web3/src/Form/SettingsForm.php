<?php

namespace Drupal\chia_web3\Form;

use Drupal\chia\Entity\Wallet;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Configure Chia web3 settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chia_web3_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['chia_web3.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $wallets = Wallet::loadMultiple();
    $walletOptions = ['' => $this->t('Select')];
    if(isset($wallets)) {
      foreach ($wallets as $wallet) {
        $walletOptions[$wallet->id()] = $wallet->label();
      }
    }
    $form['wallet'] = [
      '#type' => 'select',
      '#title' => $this->t('Wallet'),
      '#description' => $this->t("Web3 feature requires access to a wallet RPC, you have to create at least one to be able to enable this."),
      '#default_value' => $this->config('chia_web3.settings')
        ->get('wallet'),
      '#options' => $walletOptions,
    ];
    $form['enable_wallet_connect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable wallet connect'),
      '#description' => $this->t("Users will be able to connect to drupal using a chia wallet extension, like Goby wallet."),
      '#return_value' => 1,
      '#states' => [
        'disabled' => [
          'select[name="wallet"]' => ['value' => ''],
        ],
      ],
      '#default_value' => $this->config('chia_web3.settings')
        ->get('enable_wallet_connect'),
    ];
    $form['autocreate_users'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autocreate users'),
      '#description' => $this->t("Users using wallet extension connect that do not have an account will be automatically created, fake email et username will attributed, keep in mind they should be able to change theses."),
      '#return_value' => 1,
      '#states' => [
        'invisible' => [
          ':input[name="enable_wallet_connect"]' => ['checked' => FALSE],
        ],
      ],
      '#default_value' => $this->config('chia_web3.settings')
        ->get('autocreate_users'),
    ];
    $roles = Role::loadMultiple();
    $options = [];
    foreach ($roles as $role) {
      if(isset($role)) {
        $options[$role->id()] = $role->label();
      }
    }
    unset($options['anonymous']);
    unset($options['authenticated']);
    $form['wallet_connect_newly_role'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles affected to newly created user'),
      '#description' => $this->t("This role will be added to newly created user from wallet connect, leave blank if none."),
      '#options' => $options,
      '#states' => [
        'invisible' => [
          ':input[name="autocreate_users"]' => ['checked' => FALSE],
        ],
      ],
      '#default_value' => $this->config('chia_web3.settings')
        ->get('wallet_connect_newly_role') ?? [],
    ];
    $form['display_button_in_login_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display wallet connect in login form'),
      '#description' => $this->t("Wallet connect will be added to action buttons in the Drupal's login form."),
      '#return_value' => 1,
      '#states' => [
        'invisible' => [
          ':input[name="enable_wallet_connect"]' => ['checked' => FALSE],
        ],
      ],
      '#default_value' => $this->config('chia_web3.settings')
        ->get('display_button_in_login_form'),
    ];
    $form['display_link_wallet'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display link wallet to account in the user profile page'),
      '#description' => $this->t("Wallet link will be added to action buttons in the Drupal's user profile."),
      '#return_value' => 1,
      '#states' => [
        'invisible' => [
          ':input[name="enable_wallet_connect"]' => ['checked' => FALSE],
        ],
      ],
      '#default_value' => $this->config('chia_web3.settings')
        ->get('display_link_wallet'),
    ];
    $form['enable_commerce_order_payment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable chia_commerce payment via extension wallet'),
      '#return_value' => 1,
      '#default_value' => $this->config('chia_web3.settings')
        ->get('enable_commerce_order_payment') ?? 1,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('chia_web3.settings')
      ->set('wallet', $form_state->getValue('wallet'))
      ->set('enable_wallet_connect', $form_state->getValue('enable_wallet_connect'))
      ->set('autocreate_users', $form_state->getValue('autocreate_users'))
      ->set('wallet_connect_newly_role', $form_state->getValue('wallet_connect_newly_role'))
      ->set('display_button_in_login_form', $form_state->getValue('display_button_in_login_form'))
      ->set('display_link_wallet', $form_state->getValue('display_link_wallet'))
      ->set('enable_commerce_order_payment', $form_state->getValue('enable_commerce_order_payment'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
