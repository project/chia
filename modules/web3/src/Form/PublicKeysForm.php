<?php

namespace Drupal\chia_web3\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Provides a Chia web3 form.
 */
class PublicKeysForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chia_web3_public_keys';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user = User::load(\Drupal::currentUser()->id());
    $options = [];
    $publicKeys = $user->get('chia_public_keys')->getValue();
    foreach($publicKeys as $publicKey) {
      $options[$publicKey['value']] = substr($publicKey['value'], 2, 15).'...';
    }
    $form['public_keys'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Deletes public keys from this account'),
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'checkHandler']
      ],
      '#description' => $this->t('Select public keys you want to delete from this account.'),
      '#required' => FALSE,
      '#attached' => [
        'library' => ['core/drupal.dialog.ajax']
      ]
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#states' => [
        'disabled' => [
          ':input[name^="public_keys"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['actions']['link_wallet'] = [
      '#type' => 'button',
      '#value' => $this->t('Link wallet'),
      '#ajax' => [
        'callback' => [$this, 'linkWalletHandler'],
      ],
      '#attached' => [
        'library' => ['chia_web3/connect'],
        'drupalSettings' => [
          'chia_web3' => [
            'verifySignatureUrl' => Url::fromRoute('chia_web3.verify_signature')->toString(),
            'linkWalletUrl' => Url::fromRoute('chia_web3.link_wallet')->toString(),
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function linkWalletHandler(array &$form, FormStateInterface $form_state) {
    /* @var $web3 \Drupal\chia_web3\Services\Web3 */
    $web3 = \Drupal::service('chia_web3.service');
    return $web3->web3ConnectionAjaxResponse();
  }

  public function checkHandler(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $sender = $form_state->getTriggeringElement();
    $value = $form_state->getValue('public_keys');
    if($value[$sender['#return_value']]) {
      $response->addCommand(new OpenModalDialogCommand($this->t("Warning"), [
        '#markup' => $this->t("You are about to delete this public key from this account."),
      ]));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('public_keys');
    $newValues = [];
    foreach($values as $pubkey => $value) {
      if(!$value) {
        $newValues = ['value' => $pubkey];
      }
    }
    $user = User::load(\Drupal::currentUser()->id());
    $user->set('chia_public_keys', $newValues);
    $user->save();
    $this->messenger()->addStatus($this->t('Public keys has been saved.'));
  }

}
