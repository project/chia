<?php

namespace Drupal\chia_web3\Form;

use Drupal\chia_web3\Ajax\ConnectWeb3Command;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a Chia web3 form that links wallet to an already registered user.
 */
class LinkWalletForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chia_web3_link_wallet';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['link_wallet'] = [
      '#type' => 'button',
      '#value' => $this->t('Link wallet to my account on this website'),
      '#ajax' => [
        'callback' => [$this, 'submitForm'],
      ]
    ];
    $form['#attached'] = [
      'library' => ['chia_web3/connect'],
      'drupalSettings' => [
        'chia_web3' => [
          'verifySignatureUrl' => Url::fromRoute('chia_web3.verify_signature')->toString(),
          'linkWalletUrl' => Url::fromRoute('chia_web3.link_wallet')->toString(),
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /* @var $web3 \Drupal\chia_web3\Services\Web3 */
    $web3 = \Drupal::service('chia_web3.service');
    return $web3->web3ConnectionAjaxResponse();
  }
}
