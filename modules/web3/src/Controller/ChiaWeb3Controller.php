<?php

namespace Drupal\chia_web3\Controller;

use Drupal\chia_web3\Services\Web3;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Chia web3 routes.
 */
class ChiaWeb3Controller extends ControllerBase {

  /**
   * @var Web3
   */
  protected $web3;

  /**
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * @var \Drupal\externalauth\ExternalAuth
   */
  protected $externalAuth;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(Web3 $web3, MessengerInterface $messenger, ExternalAuthInterface $externalAuth) {
    $this->web3 = $web3;
    $this->messenger = $messenger;
    $this->externalAuth = $externalAuth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('chia_web3.service'),
      $container->get('messenger'),
      $container->get('externalauth.externalauth')
    );
  }

  /**
   * Builds the response.
   */
  public function verifySignature(Request $request) {
    $response = new AjaxResponse();
    $signature = $request->request->get('signature');
    $public_key = $request->request->get('public_key');
    $message = $request->getSession()->get('chia_web3_nonce');
    $isAnonymous = $this->currentUser()->isAnonymous();
    try {
      if ($this->web3->isSignatureValid($public_key, $message, $signature)) {
        $user = $this->web3->findUserByPublicKey($public_key);
        $usersDefined = $user && $user instanceof UserInterface && !$isAnonymous;
        if ($usersDefined && $user->id() !== $this->currentUser()->id()) {
          $this->messenger->addError($this->t("This public key is already being used by another account on this website. The key must be deleted from that account in order to proceed."));
        }
        else {
          if ($usersDefined && $user->id() === $this->currentUser()->id()) {
            $this->messenger->addWarning($this->t("This public key was already linked to your account, so no action was taken."));
          }
          else {
            if ($isAnonymous && !$user) {
              $username = substr($public_key, 0, 12);
              $this->externalAuth->loginRegister($username, 'chia_web3', [
                'username' => $username,
                'password' => bin2hex(openssl_random_pseudo_bytes(4)),
              ]);
            }else if ($isAnonymous && $user instanceof User) {
              $this->externalAuth->userLoginFinalize($user, $user->getAccountName(), 'chia_web3');
            }
            if (!$user) {
              $this->web3->linkPublickeyToAccount($public_key);
              $this->messenger->addStatus($this->t("The public key @public_key has been linked to your account.", [
                '@public_key' => $public_key,
              ]));
            }
          }
        }

      }
    } catch (\Exception $exc) {
      $message = $this->t("There was an error when trying to link the public key to your account: @message", [
        '@message' => $exc->getMessage(),
      ]);
      $this->getLogger('chia_web3')->error($message);
      $this->messenger->addError($message);
    }
    if($isAnonymous) {
      $response->addCommand(new RedirectCommand(Url::fromRoute('user.page')->toString()));
    } else {
      $all = $this->messenger()->all();
      $content = [
        '#type' => 'status_messages',
        '#message_list' => $this->messenger()->all()
      ];
      $response->addCommand(new OpenModalDialogCommand($this->t("Status"), $content), ['width' => 500]);
    }
    return $response;
  }

  public function login(Request $request) {
    $signature = $request->request->get('signature');
    $public_key = $request->request->get('public_key');
    $message = $request->getSession()->get('chia_web3_nonce');
  }

}
