<?php

namespace Drupal\chia_web3\Services;

use BitWasp\Bitcoin\Base58;
use BitWasp\Bitcoin\Key\Factory\PublicKeyFactory;
use BitWasp\Buffertools\Buffer;
use Drupal\chia\Entity\ChiaNode;
use Drupal\chia\Entity\Wallet;
use Drupal\chia\Services\NodeRpc;
use Drupal\chia\Services\WalletRpc;
use Drupal\chia_web3\Ajax\ConnectWeb3Command;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Service description.
 */
class Web3 {
  use StringTranslationTrait;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var Connection
   */
  protected $database;

  /**
   * @var NodeRpc
   */
  protected $nodeRpc;

  /**
   * @var \Drupal\chia\Services\WalletRpc
   */
  protected $walletRpc;

  /**
   * Constructs a Web3 object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entity_type_manager, Connection $connection, NodeRpc $nodeRpc, WalletRpc $walletRpc) {
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $connection;
    $this->nodeRpc = $nodeRpc;
    $this->walletRpc = $walletRpc;
  }

  /**
   * @return string
   *   Generates nonce
   */
  public function generateNonce() {
    $session = \Drupal::request()->getSession();
    $now = new DrupalDateTime();
    $string = $this->t("Please, sign this message in order to validate your wallet. Timestamp: @timestamp", [
      '@timestamp' => $now->getTimestamp(),
    ])->render();
    $nonce = '';
    for ($i=0; $i < strlen($string); $i++){
      $nonce .= dechex(ord($string[$i]));
    }
    $session->set('chia_web3_hexnonce', $nonce);
    $session->set('chia_web3_nonce', $string);
    return $nonce;
  }

  /**
   * Check signature validity.
   *
   * @param $public_key
   * @param $message
   * @param $signature
   *
   * @return bool
   * @throws \Drupal\chia\Exceptions\ChiaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function isSignatureValid(string $public_key, string $message, string $signature): bool {
    $wallets = Wallet::loadMultiple();
    return $this->walletRpc->isSignatureValid(reset($wallets), $public_key, $message, $signature, '');
  }

  public function generateConnectionToken() {
    $session = \Drupal::request()->getSession();
    $randomBytes = Crypt::randomBytesBase64();
    $session->set('chia_web3_connection_token', $randomBytes);
    return $randomBytes;
  }

  /**
   * @param string $public_key
   * @param \Drupal\user\UserInterface|NULL $account
   *
   * @return int
   */
  public function linkPublickeyToAccount(string $public_key, UserInterface $account = NULL): int {
    $account = $account ?? User::load(\Drupal::currentUser()->id());
    $account->get('chia_public_keys')->appendItem(['value' => $public_key]);
    return $account->save();
  }

  /**
   * Retrives user from its public key.
   *
   * @param string $public_key
   *
   * @return User|false
   */
  public function findUserByPublicKey(string $publicKey): ?User {
    $select = $this->database->select('user__chia_public_keys', 'p');
    $select->fields('p', ['entity_id']);
    $select->condition('chia_public_keys_value', $publicKey);
    $result = $select->execute();
    $stdUser = $result->fetchObject();
    $user = NULL;
    if($stdUser) {
      $user = User::load($stdUser->entity_id);
    }
    return $user;
  }

  /**
   * Initialize web3 connection.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function web3ConnectionAjaxResponse(): AjaxResponse {
    $randomBytes = $this->generateNonce();
    $response = new AjaxResponse();
    $response->addCommand(new ConnectWeb3Command($randomBytes));
    return $response;
  }

}
