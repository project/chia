(function ($, Drupal, drupalSettings) {
    //Drupal.AjaxCommands = Drupal.AjaxCommands || {prototype:{}};
    Drupal.AjaxCommands.prototype.connectWeb3Command = async function (ajax, response, status) {
        if (Drupal.chia_web3.hasExtensionWallet()) {
            const oSignature = await Drupal.chia_web3.signMessage(response.message);
            Drupal.ajax({url: drupalSettings.chia_web3.verifySignatureUrl, submit: oSignature }).execute();
        }
    }
})(jQuery, Drupal, drupalSettings);
