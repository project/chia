(function ($, drupalSettings, Drupal, window) {
    Drupal.chia_web3_commerce = false;
    Drupal.behaviors.chia_web3_commerce = {
        attach: function (context, settings) {
            if(!Drupal.chia_web3_commerce) {
                Drupal.chia_web3.pay(settings.chia_web3.to,settings.chia_web3.amount, settings.chia_web3.memos);
                Drupal.chia_web3_commerce = true;
            }
        }
    };
})(jQuery, drupalSettings, Drupal, window)