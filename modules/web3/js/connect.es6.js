(function ($, drupalSettings, Drupal, window) {
    Drupal.behaviors.chia_web3 = {
        attach: function (context, settings) {

        }
    };
    Drupal.chia_web3 = {
        hasExtensionWallet: function () {
            const {chia} = window;
            return Boolean(chia && chia.isGoby);
        },
        connect: async function () {
            try {
                await window.chia.request({method: "connect"});
                const publicKeys = await window.chia.request({method: "getPublicKeys"});
                if (publicKeys.length > 0) {
                    Drupal.ajax({"url": drupalSettings.chia_web3.sessionUrl, "publicKeys": publicKeys}).execute();
                }
            } catch (err) {
                console.log(err);
            }
        },
        signMessage: async function (message) {
            try {
                await window.chia.request({method: "connect"});
                const publicKeys = await window.chia.request({method: "getPublicKeys"});
                const signature = await window.chia.request({
                    method: "signMessage",
                    params: {"message": message, "publicKey": publicKeys[0]}
                });
                return {
                    "public_key": publicKeys[0],
                    "signature": signature,
                };
            } catch (err) {
                console.log(err);
            }
        },
        pay: async function (to, amount, memos) {
            await window.chia.request({method: "connect"});
            const params =  {
                "to": to,
                "amount": amount,
                "memos": memos,
                "assetId": ""
            };
            window.chia.request({ method: "transfer", params });
        }
    }
})(jQuery, drupalSettings, Drupal, window);