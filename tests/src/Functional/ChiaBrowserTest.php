<?php

namespace Drupal\Tests\chia\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\chia\Entity\ChiaNode;
use Drupal\chia\Entity\Wallet;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\Site\Settings;
use Drupal\file\Entity\File;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\chia\Mock\NodeRpcMock;
use Drupal\Tests\chia\Mock\WalletRpcMock;

/**
 * Test description.
 *
 * @coversDefaultClass \Drupal\chia\Controller\ChiaNodeListBuilder
 * @coversDefaultClass \Drupal\chia\Controller\ChiaWalletListBuilder
 * @coversDefaultClass \Drupal\chia\Controller\ChiaController
 * @coversDefaultClass \Drupal\chia\Form\ChiaNodeForm
 * @coversDefaultClass \Drupal\chia\Form\ChiaWalletForm
 * @coversDefaultClass \Drupal\chia\Form\ChiaWalletDeleteForm
 *
 * @group chia
 */
class ChiaBrowserTest extends BrowserTestBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system','file','chia'];

  /**
   * @var \Drupal\user\Entity\User|false
   */
  protected $admin_user;

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    $service_definition = $container->getDefinition('chia.rpc');
    $service_definition->setClass(WalletRpcMock::class);
    $service_definition = $container->getDefinition('chia.node_rpc');
    $service_definition->setClass(NodeRpcMock::class);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->admin_user = $this->drupalCreateUser(['access administration pages', 'administer chia services', 'view chia status page', 'administer chia nodes', 'administer chia wallets']);
  }

  /**
   * Test admin pages.
   */
  public function testAdminPages() {
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('admin/config/services/chia');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Chia"]');
    $this->drupalGet('admin/config/services/chia/chia-node');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "ChiaNode configuration"]');
    $this->drupalGet('admin/config/services/chia/chia-wallet');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "chia_wallet configuration"]');
    $this->drupalGet('admin/config/services/chia/status');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Chia Status"]');
  }

  /**
   * Node form test.
   *
   * @return void
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testNodeForm() {
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('admin/config/services/chia/chia-node/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Add a chianode"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="text"][@id="edit-label"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="text"][@id="edit-id"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="checkbox"][@id="edit-status"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="text"][@id="edit-node-rpc-address"]');
    $page = $this->getSession()->getPage();
    $page->fillField('edit-label', 'node');
    $page->fillField('edit-id', 'node');
    $page->checkField('edit-status');
    $page->fillField('edit-node-rpc-address', 'http://localhost:8555');
    $page->pressButton('Save');
    $node = ChiaNode::load('node');
    $this->assertEquals(TRUE, $node instanceof ChiaNode);
    $this->assertEquals('node', $node->label());
    $this->assertEquals(1, $node->get('status'));
    $this->assertEquals('http://localhost:8555', $node->get('node_rpc_address'));
  }

  /**
   * Wallet form test.
   *
   * @return void
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testWalletForm() {
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('admin/config/services/chia/chia-wallet/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "Add chia_wallet"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="text"][@id="edit-label"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="text"][@id="edit-id"]');
    $this->assertSession()->elementExists('xpath', '//input[@type="text"][@id="edit-wallet-rpc-address"]');
    $page = $this->getSession()->getPage();
    $page->fillField('edit-label', 'wallet');
    $page->fillField('edit-id', 'wallet');
    $page->fillField('edit-wallet-rpc-address', 'http://localhost:8555');
    $page->pressButton('Save');
    $wallet = Wallet::load('wallet');
    $this->assertEquals(TRUE, $wallet instanceof Wallet);
    $this->assertEquals('wallet', $wallet->label());
    $this->assertEquals('http://localhost:8555', $wallet->get('wallet_rpc_address'));
  }

  /**
   * Test chia node admin pages.
   *
   * @return void
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testNodeAdmin() {
    $this->drupalLogin($this->admin_user);
    touch('public://cert.pem');
    $fakeCert = File::create([
      'filename' => basename('cert.pem'),
      'uri' => 'public://cert.pem',
      'status' => 1,
      'uid' => 1,
    ]);
    $fakeCert->save();
    ChiaNode::create([
      'id' => 'node',
      'label' => 'node',
      'node_rpc_url' => 'http://localhost:8555',
      'status' => 1,
      'node_key' => [$fakeCert->id()],
      'node_cert' => [$fakeCert->id()],
    ])->save();
    $this->drupalGet('admin/config/services/chia/chia-node');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "ChiaNode configuration"]');
    $this->drupalGet('/admin/config/services/chia/chia-node/node/delete');
    $this->assertSession()->elementExists('xpath', '//input[@id="edit-submit"]');
    $this->getSession()->getPage()->pressButton('Delete');
    $this->assertSession()->pageTextContains('has been deleted.');
    $node = ChiaNode::load('node');
    $this->assertEquals(NULL, $node);
  }

  /**
   * Test chia wallet admin pages.
   *
   * @return void
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testWalletAdmin() {
    $this->drupalLogin($this->admin_user);
    touch('public://cert.pem');
    $fakeCert = File::create([
      'filename' => basename('cert.pem'),
      'uri' => 'public://cert.pem',
      'status' => 1,
      'uid' => 1,
    ]);
    $fakeCert->save();
    Wallet::create([
      'id' => 'wallet',
      'label' => 'wallet',
      'wallet_rpc_url' => 'http://localhost:8555',
      'wallet_key' => [$fakeCert->id()],
      'wallet_cert' => [$fakeCert->id()],
    ])->save();
    $this->drupalGet('admin/config/services/chia/chia-wallet');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//h1[text() = "chia_wallet configuration"]');
    $this->drupalGet('/admin/config/services/chia/chia-wallet/wallet/delete');
    $this->assertSession()->elementExists('xpath', '//input[@id="edit-submit"]');
    $this->getSession()->getPage()->pressButton('Delete');
    $this->assertSession()->pageTextContains('has been deleted.');
    $wallet = Wallet::load('wallet');
    $this->assertEquals(NULL, $wallet);
  }

  public function testPermissions() {
    touch('public://cert.pem');
    $fakeCert = File::create([
      'filename' => basename('cert.pem'),
      'uri' => 'public://cert.pem',
      'status' => 1,
      'uid' => 1,
    ]);
    $fakeCert->save();
    Wallet::create([
      'id' => 'wallet',
      'label' => 'wallet',
      'wallet_rpc_url' => 'http://localhost:8555',
      'wallet_key' => [$fakeCert->id()],
      'wallet_cert' => [$fakeCert->id()],
    ])->save();
    ChiaNode::create([
      'id' => 'node',
      'label' => 'node',
      'node_rpc_url' => 'http://localhost:8555',
      'status' => 1,
      'node_key' => [$fakeCert->id()],
      'node_cert' => [$fakeCert->id()],
    ])->save();
    $user = $this->createUser();
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/services/chia');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/chia/chia-node');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/chia/chia-node/add');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/admin/config/services/chia/chia-node/node/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/chia/chia-wallet');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/chia/chia-wallet/add');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('/admin/config/services/chia/chia-wallet/wallet/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/chia/status');
    $this->assertSession()->statusCodeEquals(403);
  }

}
