<?php

namespace Drupal\Tests\chia\Mock;

use Drupal\chia\Entity\Wallet;
use Drupal\chia\Exceptions\ChiaException;
use Drupal\chia\WalletRpcMethods;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\file\Entity\File;
use Faker\Factory;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class WalletRpcMock {

  /**
   * @var StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * @var Client[]
   *   The wallet client
   */
  protected array $walletClient = [];

  /**
   * Constructs a Rpc object.
   */
  public function __construct(StreamWrapperManagerInterface $streamWrapperManager) {
    $this->streamWrapperManager = $streamWrapperManager;
  }

  /**
   * Sends agnostic request to RPC server.
   *
   * @param \Drupal\chia\Entity\Wallet $wallet
   * @param string $method
   * @param array $params
   * @param string $requestMethod
   *
   * @return mixed
   * @throws \Drupal\chia\Exceptions\ChiaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function request(Wallet $wallet, string $method, array $params, string $requestMethod = 'POST'): mixed {
    $client = $this->newWalletClient($wallet);
    $response = $client->request($requestMethod, '/' . $method, [
      RequestOptions::BODY => json_encode((object) $params),
    ]);
    return json_decode($response->getBody()->getContents());
  }

  /**
   * @param Wallet|null $wallet
   *
   * @return Client
   * @throws ChiaException
   */
  public function newWalletClient(Wallet $wallet = NULL): Client {
    if (!isset($this->walletClient[$wallet->id()])) {
      if (!isset($certPath) || !isset($keyPath)) {
        $walletCert = File::load($wallet->getWalletCert());
        $walletKey = File::load($wallet->getWalletKey());
        if ($walletCert && $walletKey) {
          $certPath = $this->streamWrapperManager->getViaUri($walletCert->getFileUri())
            ->realpath();
          $keyPath = $this->streamWrapperManager->getViaUri($walletKey->getFileUri())
            ->realpath();
        }
        else {
          throw new ChiaException("Unable to get wallet client : key or cert do not exist.");
        }
      }
      $options = [
        'base_uri' => $wallet->getWalletRpcAddress(),
        RequestOptions::HEADERS => [
          'Content-Type' => 'application/json',
        ],
        RequestOptions::VERIFY => FALSE,
        RequestOptions::CONNECT_TIMEOUT => 1000,
        RequestOptions::CERT => $certPath,
        RequestOptions::SSL_KEY => $keyPath,
      ];
      $this->walletClient[$wallet->id()] = new Client($options);
    }
    return $this->walletClient[$wallet->id()];
  }


  /**
   */
  public function getWallets(Wallet $wallet) {
    $faker = Factory::create();
    return json_decode(sprintf('{"fingerprint": %s,"success": true, "wallets": [{
            "data": "",
            "id": 1,
            "name": "Chia Wallet",
            "type": 0
        }]}', $faker->randomNumber(9)));
  }

  /**
   * @param \Drupal\chia\Entity\Wallet $wallet
   *
   * @return \stdClass
   */
  public function getSyncStatus(Wallet $wallet) {
    $faker = Factory::create();
    return (object) [
      'genesis_initialized' => $faker->boolean(99),
      'success' => $faker->boolean(99),
      'synced' => $faker->boolean(99),
      'syncing' => $faker->boolean(99),
    ];
  }

  /**
   * @param Wallet $wallet
   * @param int $walletId
   *
   * @return mixed
   * @throws ChiaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getNextAddress(Wallet $wallet, int $walletId = 1): string {
    $faker = Factory::create();
    return $faker->regexify('xch[a-z0-9]{59}');
  }

  /**
   */
  public function getLastTransactions(Wallet $wallet, $walletId = 1, $toAddress = NULL) {
    $faker = Factory::create();
    return (object) [
      'success' => TRUE,
      'transactions' => [
        (object) [
          'amount' => $faker->numberBetween(0, 100),
          'confirmed' => $faker->boolean(),
          'to_address' => $toAddress,
          'wallet_id' => $walletId,
        ],
      ],
    ];
  }

  /**
   * @param $ids
   *
   * @return array
   */
  public function getWalletStates($ids = NULL): array {
    $wallets = Wallet::loadMultiple($ids);
    $newWalletState = [];
    foreach ($wallets as $wallet) {
      try {
        $status = $this->getSyncStatus($wallet);
        $newWalletState[$wallet->id()] = $status->success ? 'ok' : 'warning';
      } catch (\Exception $exc) {
        $newWalletState[$wallet->id()] = 'error';
      }
    }
    return $newWalletState;
  }

}