<?php

namespace Drupal\Tests\chia\Kernel;

use Drupal\chia\Entity\ChiaNode;
use Drupal\chia\Entity\Wallet;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test description.
 *
 * @coversDefaultClass \Drupal\chia\Entity\ChiaNode
 * @coversDefaultClass \Drupal\chia\Entity\Wallet
 *
 * @group chia
 */
class ConfigEntityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['chia'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['chia']);
    $this->installEntitySchema('chia_node');
    $this->installEntitySchema('chia_wallet');
  }

  /**
   * Test node.
   */
  public function testNode() {
    $node = ChiaNode::create([
      'id' => 'node',
      'label' => 'node',
      'node_rpc_address' => 'http://node:8555',
      'node_is_public' => 0,
      'node_cert' => ['target_id' => 0],
      'node_key' => ['target_id' => 0],
    ]);
    try {
      $node->save();
    } catch (EntityStorageException $exc) {
      print $exc->getMessage();
    }
    $chiaNode = ChiaNode::load('node');
    $this->assertEquals('node', $chiaNode->id());
    $this->assertEquals('node', $chiaNode->label());
    $this->assertEquals('http://node:8555', $chiaNode->get('node_rpc_address'));
    $this->assertEquals(0, $chiaNode->get('node_is_public'));
    $this->assertEquals(0, $chiaNode->get('node_cert')['target_id']);
    $this->assertEquals(0, $chiaNode->get('node_key')['target_id']);
  }

  /**
   * Test wallet.
   */
  public function testWallet() {
    $wallet = Wallet::create([
      'id' => 'wallet',
      'label' => 'wallet',
      'wallet_rpc_address' => 'http://wallet:8555',
      'wallet_cert' => ['target_id' => 2],
      'wallet_key' => ['target_id' => 3],
    ]);
    try {
      $wallet->save();
    } catch (EntityStorageException $exc) { }
    $wallet = Wallet::load('wallet');
    $this->assertEquals('wallet', $wallet->label());
    $this->assertEquals('http://wallet:8555', $wallet->get('wallet_rpc_address'));
    $this->assertEquals(2, $wallet->get('wallet_cert')['target_id']);
    $this->assertEquals(3, $wallet->get('wallet_key')['target_id']);
  }

}
