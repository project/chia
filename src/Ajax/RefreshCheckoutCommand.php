<?php

namespace Drupal\chia\Ajax;

class RefreshCheckoutCommand implements \Drupal\Core\Ajax\CommandInterface
{

  /**
   * @inheritDoc
   */
  public function render()
  {
    return [
      'command' => 'refreshCheckout',
    ];
  }
}
