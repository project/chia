<?php

namespace Drupal\chia;

class WalletRpcMethods {
  const GET_NEXT_ADDRESS = 'get_next_address';
  const GET_TRANSACTIONS = 'get_transactions';
  const VERIFY_SIGNATURE = 'verify_signature';
}