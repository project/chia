<?php

namespace Drupal\chia\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Example entity.
 */
interface WalletInterface extends ConfigEntityInterface {

}
