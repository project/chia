<?php

namespace Drupal\chia\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\file\Entity\File;

/**
 * Defines the Chia wallet entity.
 *
 * @ConfigEntityType(
 *   id = "chia_wallet",
 *   label = @Translation("Chia wallet"),
 *   handlers = {
 *     "list_builder" = "Drupal\chia\Controller\ChiaWalletListBuilder",
 *     "form" = {
 *       "add" = "Drupal\chia\Form\ChiaWalletForm",
 *       "edit" = "Drupal\chia\Form\ChiaWalletForm",
 *       "delete" = "Drupal\chia\Form\ChiaWalletDeleteForm",
 *     }
 *   },
 *   config_prefix = "chia_wallet",
 *   admin_permission = "administer chia wallets",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "wallet_rpc_address" = "wallet_rpc_address",
 *     "wallet_cert" = "wallet_cert",
 *     "wallet_key" = "wallet_key",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "wallet_rpc_address",
 *     "wallet_cert",
 *     "wallet_key",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/services/chia/chia-wallet/{chia_wallet}",
 *     "delete-form" =
 *   "/admin/config/services/chia/chia-wallet/{chia_wallet}/delete",
 *   }
 * )
 */
class Wallet extends ConfigEntityBase implements WalletInterface, RpcInterface {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $label;

  protected $wallet_rpc_address;

  protected $wallet_cert;

  protected $wallet_key;

  /**
   * @return mixed
   */
  public function getWalletRpcAddress(): ?string {
    return $this->wallet_rpc_address;
  }

  /**
   * @param mixed $wallet_rpc_address
   */
  public function setWalletRpcAddress($wallet_rpc_address): void {
    $this->wallet_rpc_address = $wallet_rpc_address;
  }

  /**
   * @return int
   */
  public function getWalletCert(): ?int {
    return isset($this->wallet_cert) && count($this->wallet_cert) ? $this->wallet_cert[0] : NULL;
  }

  public function getWalletCertPath(): ?string {
    $fid = $this->getWalletCert();
    if (isset($fid)) {
      $file = File::load($this->getWalletCert());
    }
    return isset($file) && $file ? $file->getFileUri() : NULL;
  }

  public function getWalletKeyPath(): ?string {
    $fid = $this->getWalletKey();
    if (isset($fid)) {
      $file = File::load($this->getWalletKey());
    }
    return isset($file) && $file ? $file->getFileUri() : NULL;
  }


  /**
   * @return int
   */
  public function getWalletKey(): ?int {
    return isset($this->wallet_key) && count($this->wallet_key) ? $this->wallet_key[0] : NULL;
  }

  /**
   * @return string|null
   */
  public function getRpcAddress(): ?string {
    return $this->getWalletRpcAddress();
  }


}
