<?php

namespace Drupal\chia\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\file\Entity\File;

/**
 * Defines the chianode entity type.
 *
 * @ConfigEntityType(
 *   id = "chia_node",
 *   label = @Translation("Chia node"),
 *   label_collection = @Translation("Nodes"),
 *   label_singular = @Translation("Chia node"),
 *   label_plural = @Translation("Chia nodes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count chia node",
 *     plural = "@count chia nodes",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\chia\Controller\ChiaNodeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\chia\Form\ChiaNodeForm",
 *       "edit" = "Drupal\chia\Form\ChiaNodeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "chia_node",
 *   admin_permission = "administer chia nodes",
 *   links = {
 *     "collection" = "/admin/config/services/chia/chia-node",
 *     "add-form" = "/admin/config/services/chia/chia-node/add",
 *     "edit-form" = "/admin/config/services/chia/chia-node//{chia_node}",
 *     "delete-form" =
 *   "/admin/config/services/chia/chia-node/{chia_node}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "node_rpc_address",
 *     "node_is_public",
 *     "node_cert",
 *     "node_key",
 *   }
 * )
 */
class ChiaNode extends ConfigEntityBase implements ChiaNodeInterface, RpcInterface {

  /**
   * The chianode ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The chianode label.
   *
   * @var string
   */
  protected $label;

  /**
   * The chianode status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The chia_node description.
   *
   * @var string
   */
  protected $description;

  /**
   * @var string
   */
  protected $node_rpc_address;

  /**
   * @var bool
   */
  protected $node_is_public;

  /**
   * @var string
   */
  protected $node_cert;

  /**
   * @var string
   */
  protected $node_key;

  /**
   * @return mixed
   */
  public function getNodeRpcAddress(): ?string {
    return $this->node_rpc_address;
  }

  public function getRpcAddress(): ?string {
    return $this->getNodeRpcAddress();
  }

  /**
   * @param mixed $node_rpc_address
   */
  public function setNodeRpcAddress($node_rpc_address): void {
    $this->node_rpc_address = $node_rpc_address;
  }

  /**
   * @return int
   */
  public function getNodeCert(): ?int {
    return isset($this->node_cert) && count($this->node_cert) ? $this->node_cert[0] : NULL;
  }

  public function getNodeCertPath(): ?string {
    $fid = $this->getNodeCert();
    if (isset($fid)) {
      $file = File::load($fid);
    }
    return isset($file) && $file ? $file->getFileUri() : NULL;
  }

  public function getNodeKeyPath(): ?string {
    $fid = $this->getNodeKey();
    if (isset($fid)) {
      $file = File::load($fid);
    }
    return isset($file) && $file ? $file->getFileUri() : NULL;
  }


  /**
   * @return int
   */
  public function getNodeKey(): ?int {
    return isset($this->node_key) && count($this->node_key) ? $this->node_key[0] : NULL;
  }

  /**
   * @param bool $node_is_public
   */
  public function setNodeIsPublic(bool $node_is_public): void {
    $this->node_is_public = $node_is_public;
  }

  /**
   * @return bool
   */
  public function isNodeIsPublic(): bool {
    return $this->node_is_public;
  }


}
