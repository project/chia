<?php

namespace Drupal\chia\Entity;

interface RpcInterface {
  public function getRpcAddress(): ?string;
}