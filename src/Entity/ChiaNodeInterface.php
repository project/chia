<?php

namespace Drupal\chia\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a chianode entity type.
 */
interface ChiaNodeInterface extends ConfigEntityInterface {

}
