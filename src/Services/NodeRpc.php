<?php

namespace Drupal\chia\Services;

use Drupal\chia\Entity\ChiaNode;
use Drupal\chia\Exceptions\ChiaException;
use Drupal\chia\NodeRpcMethods;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Service description.
 */
class NodeRpc {

  use StringTranslationTrait;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  protected array $nodeClient = [];

  /**
   * Constructs a NodeRpc object.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * Sends agnostic request to RPC server.
   *
   * @param \Drupal\chia\Entity\ChiaNode $node
   * @param string $method
   * @param array $params
   * @param string $requestMethod
   *
   * @return mixed
   * @throws \Drupal\chia\Exceptions\ChiaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function request(ChiaNode $node, string $method, array $params, string $requestMethod = 'POST'): mixed {
    $client = $this->newNodeClient($node);
    $response = $client->request($requestMethod, '/' . $method, [
      RequestOptions::BODY => json_encode((object) $params),
    ]);
    return json_decode($response->getBody()->getContents());
  }

  /**
   * @param ChiaNode|null $node
   *
   * @return Client
   * @throws ChiaException
   */
  public function newNodeClient(ChiaNode $node = NULL): Client {
    if (!isset($this->nodeClient[$node->id()])) {
      $certId = $node->getNodeCert();
      $keyId = $node->getNodeKey();
      if (isset($certId) || isset($keyId)) {
        $nodeCert = File::load($certId);
        $nodeKey = File::load($keyId);
        if ($nodeCert && $nodeKey) {
          $certPath = $this->streamWrapperManager->getViaUri($nodeCert->getFileUri())
            ->realpath();
          $keyPath = $this->streamWrapperManager->getViaUri($nodeKey->getFileUri())
            ->realpath();
        }
        else {
          throw new ChiaException("Unable to get node client : key or cert do not exist.");
        }
      }
      $options = [
        'base_uri' => $node->getNodeRpcAddress(),
        RequestOptions::HEADERS => [
          'Content-Type' => 'application/json',
        ],
        RequestOptions::VERIFY => FALSE,
        RequestOptions::CONNECT_TIMEOUT => 1000,
      ];
      if (isset($certPath) && isset($keyPath)) {
        $options[RequestOptions::CERT] = $certPath;
        $options[RequestOptions::SSL_KEY] = $keyPath;
      }
      $this->nodeClient[$node->id()] = new Client($options);
    }
    return $this->nodeClient[$node->id()];
  }

  /**
   * Method description.
   */
  public function getNetworkInfo(ChiaNode $node) {
    return $this->request($node, NodeRpcMethods::GET_NETWORK_INFO, []);

  }


  public function getNodesStates($ids = NULL): array {
    $nodes = ChiaNode::loadMultiple($ids);
    $newNodeState = [];
    foreach ($nodes as $node) {
      try {
        $status = $this->getNetworkInfo($node);
        $newNodeState[$node->id()] = $status->success ? 'ok' : 'warning';
      } catch (\Exception $exc) {
        $newNodeState[$node->id()] = 'error';
      }
    }
    return $newNodeState;
  }

}
