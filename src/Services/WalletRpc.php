<?php

namespace Drupal\chia\Services;

use Drupal\chia\ChiaConstants;
use Drupal\chia\Entity\Wallet;
use Drupal\chia\Exceptions\ChiaException;
use Drupal\chia\WalletRpcMethods;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\file\Entity\File;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

/**
 * Service description.
 */
class WalletRpc {

  /**
   * @var StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * @var Client[]
   *   The wallet client
   */
  protected array $walletClient = [];

  /**
   * Constructs a Rpc object.
   */
  public function __construct(StreamWrapperManagerInterface $streamWrapperManager) {
    $this->streamWrapperManager = $streamWrapperManager;
  }

  /**
   * Sends agnostic request to RPC server.
   *
   * @param \Drupal\chia\Entity\Wallet $wallet
   * @param string $method
   * @param array $params
   * @param string $requestMethod
   *
   * @return mixed
   * @throws \Drupal\chia\Exceptions\ChiaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function request(Wallet $wallet, string $method, array $params, string $requestMethod = 'POST'): mixed {
    $client = $this->newWalletClient($wallet);
    $response = $client->request($requestMethod, '/' . $method, [
      RequestOptions::BODY => json_encode((object) $params),
    ]);
    return json_decode($response->getBody()->getContents());
  }

  /**
   * @param Wallet|null $wallet
   *
   * @return Client
   * @throws ChiaException
   */
  public function newWalletClient(Wallet $wallet = NULL): Client {
    if (!isset($this->walletClient[$wallet->id()])) {
      $certId = $wallet->getWalletCert();
      $keyId = $wallet->getWalletKey();
      if (isset($certId) && isset($keyId)) {
        $walletCert = File::load($certId);
        $walletKey = File::load($keyId);
        if ($walletCert && $walletKey) {
          $certPath = $this->streamWrapperManager->getViaUri($walletCert->getFileUri())
            ->realpath();
          $keyPath = $this->streamWrapperManager->getViaUri($walletKey->getFileUri())
            ->realpath();
        }
        else {
          throw new ChiaException("Unable to get wallet client : key or cert do not exist.");
        }
      }
      $options = [
        'base_uri' => $wallet->getWalletRpcAddress(),
        RequestOptions::HEADERS => [
          'Content-Type' => 'application/json',
        ],
        RequestOptions::VERIFY => FALSE,
        RequestOptions::CONNECT_TIMEOUT => 1000,
      ];
      if (isset($certPath) && isset($keyPath)) {
        $options[RequestOptions::CERT] = $certPath;
        $options[RequestOptions::SSL_KEY] = $keyPath;
      }
      $this->walletClient[$wallet->id()] = new Client($options);
    }
    return $this->walletClient[$wallet->id()];
  }


  /**
   * @throws GuzzleException
   * @throws ChiaException
   */
  public function getWallets(Wallet $wallet) {
    return $this->request($wallet, 'get_wallets', [
      'include_data' => TRUE,
      'type' => 0,
    ]);
  }

  /**
   * @param \Drupal\chia\Entity\Wallet $wallet
   *
   * @return array
   */
  public function getSyncStatus(Wallet $wallet) {
    return $this->request($wallet, 'get_sync_status', []);
  }

  /**
   * @param Wallet $wallet
   * @param int $walletId
   *
   * @return mixed
   * @throws ChiaException
   * @throws GuzzleException
   */
  public function getNextAddress(Wallet $wallet, $walletId = 1): string {
    $result = $this->request($wallet, WalletRpcMethods::GET_NEXT_ADDRESS, [
      'wallet_id' => (int) $walletId,
      'new_address' => TRUE,
    ]);
    // If failed, then get last used address
    if (!$result->success) {
      $result = $this->request($wallet, WalletRpcMethods::GET_NEXT_ADDRESS, [
        'wallet_id' => (string) $walletId,
        'new_address' => FALSE,
      ]);
    }
    return $result->address;
  }

  /**
   * @throws GuzzleException|ChiaException
   */
  public function getLastTransactions(Wallet $wallet, $walletId = 1, $toAddress = NULL) {
    $params = [
      'start' => 0,
      'end' => 50,
      'reverse' => TRUE,
      'wallet_id' => $walletId,
    ];
    if (isset($toAddress)) {
      $params['to_address'] = $toAddress;
    }
    return $this->request($wallet, WalletRpcMethods::GET_TRANSACTIONS, $params);
  }

  /**
   * Checks signature.
   *
   * @param \Drupal\chia\Entity\Wallet $wallet
   * @param string $public_key
   * @param string $message
   *   Plain message.
   * @param string $signature
   * @param string $address
   *   Address mark as required in the doc.
   *
   * @return bool
   * @throws \Drupal\chia\Exceptions\ChiaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function isSignatureValid(Wallet $wallet, string $public_key, string $message, string $signature, string $address): bool {
    $params = [
      'pubkey' => $public_key,
      'message' => $this->computeMessageForValidation($message),
      'signature' => $signature,
    ];
    $response = $this->request($wallet, WalletRpcMethods::VERIFY_SIGNATURE, $params);
    return $response->isValid;
  }

  /**
   * Given a clear message, returns a hexadecimal string hashed.
   *
   * @see https://docs.chia.net/wallet-rpc/#verify_signature
   * @see https://github.com/Chia-Network/chia-blockchain/blob/main/chia/rpc/wallet_rpc_api.py#L1252
   * @see https://github.com/Chia-Network/chips/blob/main/CHIPs/chip-0002.md#signmessage
   * @see keybase://chat/chia_network.public#support/173317
   *
   * Thanks @rigidity
   *
   * @param string $message
   *   The clear message.
   *
   * @return string
   *   Hexadecimal string that can be verified.
   */
  public function computeMessageForValidation(string $message) {
    $prefix = hash('sha256', "\x01" . ChiaConstants::MESSAGE_PREFIX, TRUE);
    $message = hash('sha256', "\x01$message", TRUE);
    $finalMessage = hash('sha256', "\x02$prefix$message", TRUE);
    return bin2hex($finalMessage);
  }


  /**
   * @param $ids
   *
   * @return array
   */
  public function getWalletStates($ids = NULL): array {
    $wallets = Wallet::loadMultiple($ids);
    $newWalletState = [];
    foreach ($wallets as $wallet) {
      try {
        $status = $this->getSyncStatus($wallet);
        $newWalletState[$wallet->id()] = $status->success ? 'ok' : 'warning';
      } catch (\Exception $exc) {
        $newWalletState[$wallet->id()] = 'error';
      }
    }
    return $newWalletState;
  }

}
