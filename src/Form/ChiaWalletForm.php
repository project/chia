<?php

namespace Drupal\chia\Form;

use Drupal\chia\Services\WalletRpc;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class ChiaWalletForm extends EntityForm
{

  /**
   * Constructs an ExampleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * @var WalletRpc
   */
  protected $rpc;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, WalletRpc $rpc)
  {
    $this->entityTypeManager = $entityTypeManager;
    $this->rpc = $rpc;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('chia.rpc')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state)
  {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t("Label for the Example."),
      '#required' => TRUE,
    ];

    $form['wallet_rpc_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wallet address'),
      '#description' => $this->t("Wallet address, can be localhost if chia node if running on the same machine."),
      '#default_value' => $this->entity->getWalletRpcAddress() ?? 'https://localhost:9256',
    ];

    $form['wallet_cert'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Wallet cert'),
      '#description' => $this->t("Wallet cert, will be uploaded to private files (this file can be found in ~/.chia/mainnet/config/ssl/wallet/private_wallet.crt), be aware if this file get stolen, you can loose all your chia."),
      '#upload_validators' => [
        'file_validate_extensions' => ['crt', 'pem'],
      ],
      '#upload_location' => 'private://chia',
      '#default_value' => $this->entity->getWalletCert() ? [$this->entity->getWalletCert()] : '',
    ];

    $form['wallet_key'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Wallet key'),
      '#description' => $this->t("Wallet key, will be uploaded to private files (this file can be found in ~/.chia/mainnet/config/ssl/wallet/private_wallet.key), be aware if this file get stolen, you can loose all your chia."),
      '#upload_validators' => [
        'file_validate_extensions' => ['key', 'pem'],
      ],
      '#upload_location' => 'private://chia',
      '#default_value' => $this->entity->getWalletKey() ? [$this->entity->getWalletKey()] : '',
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $status = $this->entity->save();
    $walletCert = $form_state->getValue('wallet_cert');
    if ($walletCert) {
      $walletCertFile = File::load($walletCert[0]);
      if ($walletCertFile) {
        $walletCertFile->setPermanent();
        $walletCertFile->save();
      }
    }
    $walletKey = $form_state->getValue('wallet_key');
    if ($walletKey) {
      $walletKeyFile = File::load($walletKey[0]);
      if ($walletKeyFile) {
        $walletKeyFile->setPermanent();
        $walletKeyFile->save();
      }
    }
    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label chia wallet created.', [
        '%label' => $this->entity->label(),
      ]));
    } else {
      $this->messenger()->addMessage($this->t('The %label chia wallet updated.', [
        '%label' => $this->entity->label(),
      ]));
    }
    $form_state->setRedirect('entity.chia_wallet.collection');
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id)
  {
    $entity = $this->entityTypeManager->getStorage('chia_wallet')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
