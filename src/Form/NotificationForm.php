<?php

namespace Drupal\chia\Form;

use Drupal\chia\Entity\Wallet;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Chia form.
 */
class NotificationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    /**
     * This trick allow to display same form on same page several times.
     * @see https://www.drupal.org/project/drupal/issues/2821852#comment-13117654
     **/
    static $count = 0;
    return 'chia_notification_'.++$count;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $object = NULL) {
    $form['#object'] = $object;
    $sequence_id = $form['#object'] instanceof Wallet? 'wallet_notifications' : 'node_notifications';
    $notifications = \Drupal::configFactory()->get('chia.settings')->get($sequence_id);
    $notifications = isset($notifications) ? $notifications : [];
    $form['notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notification on status change'),
      '#return_value' => $object->id(),
      '#default_value' => in_array($object->id(), $notifications) ? $object->id() : 0,
      '#ajax' => [
        'callback' => [$this, 'submitForm']
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('message')) < 10) {
      $form_state->setErrorByName('message', $this->t('Message should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sequence_id = $form['#object'] instanceof Wallet? 'wallet_notifications' : 'node_notifications';
    $notifications = \Drupal::configFactory()->get('chia.settings')->get($sequence_id);
    $notifications = isset($notifications) ? $notifications : [];
    $value = $form_state->getValue('notification');
    if($value && !in_array($value, $notifications)) {
      $notifications[] = $value;
    } else if (($key = array_search($value, $notifications)) !== false) {
      unset($notifications[$key]);
    }
    \Drupal::configFactory()->getEditable('chia.settings')->set($sequence_id, $notifications)->save(TRUE);
  }

}
