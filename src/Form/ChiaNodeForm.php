<?php

namespace Drupal\chia\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * ChiaNode form.
 *
 * @property \Drupal\chia\Entity\ChiaNodeInterface $entity
 */
class ChiaNodeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the chianode.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\chia\Entity\ChiaNode::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the chianode.'),
    ];


    $form['node_rpc_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node address'),
      '#description' => $this->t("Node address, can be localhost if chia node if running on the same machine."),
      '#default_value' => $this->entity->getNodeRpcAddress() ?? 'https://localhost:8555',
    ];

    $form['node_cert'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Node cert'),
      '#description' => $this->t("Node cert, will be uploaded to private files (this file can be found in ~/.chia/mainnet/config/ssl/full_node/private_full_node.crt), be aware if this file get stolen, you can loose all your chia."),
      '#upload_validators' => [
        'file_validate_extensions' => ['crt', 'pem'],
      ],
      '#upload_location' => 'private://chia',
      '#default_value' => $this->entity->getNodeCert() ? [$this->entity->getNodeCert()] : '',
    ];

    $form['node_key'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Node key'),
      '#description' => $this->t("Node key, will be uploaded to private files (this file can be found in ~/.chia/mainnet/config/ssl/full_node/private_full_node.key), be aware if this file get stolen, you can loose all your chia."),
      '#upload_validators' => [
        'file_validate_extensions' => ['key', 'pem'],
      ],
      '#upload_location' => 'private://chia',
      '#default_value' => $this->entity->getNodeKey() ? [$this->entity->getNodeKey()] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new chia node %label.', $message_args)
      : $this->t('Updated chia node %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
