<?php

namespace Drupal\chia\EventSubscriber;

use Drupal\chia\Entity\Wallet;
use Drupal\chia\Events\ChiaStates;
use Drupal\chia\Services\NodeRpc;
use Drupal\chia\Services\WalletRpc;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Chia event subscriber.
 */
class ChiaStateSubscriber implements EventSubscriberInterface {

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * @var \Drupal\chia\Services\NodeRpc
   */
  protected NodeRpc $nodeRpc;

  /**
   * @var WalletRpc
   */
  protected WalletRpc $walletRpc;

  /**
   * Constructs a ChiaStateSubscriber object.
   *
   * @param NodeRpc
   *   Node RPC service.
   * @param WalletRpc
   *   Wallet RPC service.
   */
  public function __construct(NodeRpc $nodeRpc, WalletRpc $walletRpc) {
    $this->nodeRpc = $nodeRpc;
    $this->walletRpc = $walletRpc;
  }

  /**
   * Kernel response event handler.
   *
   * @param ChiaStates $event
   *   ChiaStates event.
   */
  public function onChiaStatesUpdate(ChiaStates $event) {
    $states = $event->getOldStates();
    foreach ($event->getNewStates() as $subjectId => $status) {
      if (!isset($states) || !isset($states[$subjectId]) || $states[$subjectId] !== $status) {
        $logLevel = $status === 'ok' ? 'info' : $status;
        $type = $event->getSubjects()[$subjectId] instanceof Wallet? 'wallet' : 'node';
        $notifications = \Drupal::config('chia.settings')->get(sprintf('%s_notifications', $type));
        $message =  t("Status changed for @type @name running on @rpc : @status", [
          '@type' => $type,
          '@name' => $event->getSubjects()[$subjectId]->label(),
          '@rpc' => $event->getSubjects()[$subjectId]->getRpcAddress(),
          '@status' => $status,
        ]);
        if(in_array($subjectId, $notifications ?? [])) {
          $event->addMessage($logLevel, $message);
        }
        \Drupal::logger('chia')->log($logLevel, $message);
      }
    }
    \Drupal::state()->set(sprintf('chia_%s_states', $event->getType()), $event->getNewStates());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ChiaStates::EVENT_NAME => ['onChiaStatesUpdate'],
    ];
  }

}
