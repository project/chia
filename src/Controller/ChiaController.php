<?php

namespace Drupal\chia\Controller;

use Drupal\chia\Entity\ChiaNode;
use Drupal\chia\Entity\Wallet;
use Drupal\chia\Form\NotificationForm;
use Drupal\chia\Services\NodeRpc;
use Drupal\chia\Services\WalletRpc;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Chia routes.
 */
class ChiaController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The chia.rpc service.
   *
   * @var WalletRpc
   */
  protected $rpc;

  /**
   * @var \Drupal\chia\Services\NodeRpc
   */
  protected $nodeRpc;

  /**
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The controller constructor.
   *
   * @param WalletRpc $rpc
   *   The chia.rpc service.
   */
  public function __construct(WalletRpc $rpc, NodeRpc $nodeRpc, FormBuilderInterface $formBuilder) {
    $this->rpc = $rpc;
    $this->nodeRpc = $nodeRpc;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('chia.rpc'),
      $container->get('chia.node_rpc'),
      $container->get('form_builder')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $wallets = Wallet::loadMultiple();
    $walletArr = [
      'wallets' => [],
    ];
    if (count($wallets)) {
      foreach ($wallets as $wallet) {
        $status = 'error';
        $form = $this->formBuilder->getForm(NotificationForm::class, $wallet);
        try {
          $walletStatus = $this->rpc->getSyncStatus($wallet);
          $status = $walletStatus->synced ? 'checked' : 'warning';
          $statusText = $walletStatus->synced ? $this->t('Wallet is synchronised') : '';
          if (!$walletStatus->synced) {
            $statusText = $walletStatus->syncing ? $this->t('Wallet is synchronizing') : $this->t('Wallet is NOT synchronizing');
            $status = 'warning';
          }
          $walletArr['wallets'][] = [
            '#theme' => 'status_report_counter',
            '#amount' => 1,
            '#text' => [
              'infos' => [
                '#markup' => $statusText,
              ],
              'form' => $form,
            ],
            '#severity' => $status,
            '#weight' => 0,
          ];
        } catch (\Exception $exc) {
          $walletArr['wallets'][] = [
            '#theme' => 'status_report_counter',
            '#amount' => 1,
            '#text' => [
              'infos' => [
                '#markup' => $this->t('Unable to contact wallet RPC @wallet_name, check wallet is running on @wallet_rpc_host', [
                    '@wallet_name' => $wallet->label(),
                    '@wallet_rpc_host' => $wallet->getWalletRpcAddress(),
                  ]
                ),
              ],
              'input' => $form,
            ],
            '#severity' => 'error',
            '#weight' => 0,
          ];
        }
      }
    }
    else {
      $walletArr['wallets'][] = [
        '#theme' => 'status_report_counter',
        '#amount' => 1,
        '#text' => $this->t('No wallet have been configured yet, would you like to @add_wallet ?', [
          '@add_wallet' => Link::fromTextAndUrl($this->t('add wallet'), Url::fromRoute('entity.chia_wallet.add_form'))->toString(),
        ]),
        '#severity' => 'error',
        '#weight' => 0,
      ];
    }

    $nodes = ChiaNode::loadMultiple();
    $nodeArr = [
      'nodes' => [],
    ];
    if (count($nodes)) {
      foreach ($nodes as $node) {
        $form = $this->formBuilder->getForm(NotificationForm::class, $node);
        try {
          $nodeStatus = $this->nodeRpc->getNetworkInfo($node);
          $status = $walletStatus->success ? 'checked' : 'warning';
          $statusText = $this->t('Node is running on @network_name', [
            '@network_name' => $nodeStatus->network_name,
          ]);
          $nodeArr['nodes'][] = [
            '#theme' => 'status_report_counter',
            '#amount' => 1,
            '#text' => [
              'infos' => [
                '#markup' => $statusText,
              ],
              'form' => $form,
            ],
            '#severity' => $status,
            '#weight' => 0,
          ];

        } catch (\Exception $exc) {
          $nodeArr['nodes'][] = [
            '#theme' => 'status_report_counter',
            '#amount' => 1,
            '#text' => [
              'infos' => [
                '#markup' => $this->t('Unable to contact full node RPC @node_name, check node is running on @node_rpc_host', [
                  '@node_name' => $node->label(),
                  '@node_rpc_host' => $node->getNodeRpcAddress(),
                ]),
              ],
              'input' => $form,
            ],
            '#severity' => 'error',
            '#weight' => 0,
          ];
        }
      }
    }
    $report['status_report_page']['wallet_title'] = [
      '#markup' => '<h2>' . $this->t("Wallets") . '</h2>',
    ];
    $report['status_report_page']['wallets'] = [
      '#theme' => 'status_report_page',
      '#counters' => $walletArr,
      '#general_info' => [
        '#markup' => $this->t("Chia wallets created."),
      ],
    ];
    $report['status_report_page']['node_title'] = [
      '#markup' => '<h2>' . $this->t("Nodes") . '</h2>',
    ];
    $report['status_report_page']['nodes'] = [
      '#theme' => 'status_report_page',
      '#counters' => $nodeArr,
      '#general_info' => [
        '#markup' => $this->t("Chia nodes created."),
      ],
    ];
    return $report;
  }

  public function notificationCallbackHandler($form, $form_state) {

  }

}
