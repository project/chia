<?php

namespace Drupal\chia\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of chianodes.
 */
class ChiaNodeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\chia\ChiaNodeInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    /* @var $rpc \Drupal\chia\Services\NodeRpc */
    $rpc = \Drupal::service('chia.node_rpc');

    try {
      $result = $rpc->getNetworkInfo($entity);
      $row['status'] = $this->t('OK : @network_name', ['@network_name' => $result->network_name]);
    }catch(\Exception $exc) {
      $row['status'] = $this->t('Error');
    }
    return $row + parent::buildRow($entity);
  }

}
