<?php

namespace Drupal\chia\Controller;

use Drupal\chia\Services\WalletRpc;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class ChiaWalletListBuilder extends ConfigEntityListBuilder
{
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Machine name');
    $header['label'] = $this->t('Chia wallet');
    $header['wallet_rpc_address'] = $this->t('Wallet RPC address');
    $header['wallet_cert'] = $this->t('Cert');
    $header['wallet_key'] = $this->t('Key');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['wallet_rpc_address'] = $entity->getWalletRpcAddress();
    $row['wallet_cert'] = $entity->getWalletCertPath();
    $row['wallet_key'] = $entity->getWalletKeyPath();
    /* @var $rpc WalletRpc */
    $rpc = \Drupal::service('chia.rpc');
    try {
      $wallets = $rpc->getWallets($entity);
      $row['status'] = sprintf('OK : %s', $wallets->fingerprint);
    } catch(\Exception $e) {
      $row['status'] = 'ERROR';
    }
    return $row + parent::buildRow($entity);
  }
}
