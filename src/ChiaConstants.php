<?php

namespace Drupal\chia;

class ChiaConstants
{
  // XCH wallet types
  const WALLET_TYPE_CAT = 0;
  const WALLET_TYPE_DID = 1;
  const WALLET_TYPE_NFT = 2;
  const WALLET_TYPE_POOL = 3;

  const MESSAGE_PREFIX = 'Chia Signed Message';
}
