<?php

namespace Drupal\chia\Events;

use Drupal\chia\Entity\ChiaNode;
use Drupal\chia\Entity\Wallet;
use Drupal\Component\EventDispatcher\Event;

class ChiaStates extends Event {

  const EVENT_NAME = 'CHIA_STATE_EVENT';

  /**
   * @var array
   */
  private ?array $oldStates;

  /**
   * @var array
   */
  private array $newStates;

  /**
   * @var \Drupal\chia\Entity\RpcInterface[]
   */
  private array $subjects = [];

  /**
   * @var array
   */
  private array $messageRenderArray;

  private string $type;

  /**
   * @param array $newStates
   */
  public function __construct(array $newStates, string $type) {
    $szState = $type === 'wallet' ? 'chia_wallet_states' : 'chia_node_states';
    $this->oldStates = \Drupal::state()->get($szState, []);
    $this->newStates = $newStates;
    $class = $type === 'wallet' ? Wallet::class : ChiaNode::class;
    $this->subjects = $class::loadMultiple();
    $this->messageRenderArray = [
      '#prefix' => '<h2>'.t(ucfirst($type).'s').'</h2>',
      'content' => [],
      '#alerts' => [],
    ];
    $this->type = $type;
  }

  /**
   * @return array
   */
  public function getOldStates(): array {
    return $this->oldStates;
  }

  /**
   * @return array
   */
  public function getNewStates(): array {
    return $this->newStates;
  }

  /**
   * @return \Drupal\chia\Entity\ChiaNode[]|\Drupal\chia\Entity\Wallet[]
   */
  public function getSubjects(): array {
    return $this->subjects;
  }

  /**
   * Add message to render array.
   *
   * @param $type
   * @param $message
   *
   * @return void
   */
  public function addMessage($type, $message): void {
    $this->messageRenderArray['content'][] = [
      '#markup' => $message,
    ];
    $this->messageRenderArray['#alerts'][] = [
      '#type' => $type,
      '#message' => $message,
    ];
  }

  /**
   * @return array
   */
  public function getMessageRenderArray(): array {
    return $this->messageRenderArray;
  }

  /**
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

}
